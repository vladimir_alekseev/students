$(function(){
	initPopupMess()
	initScrollTo()
	try{
		window.scrollReveal = new scrollReveal();
	}catch(e){}
	var slider = new xSlider;
	slider.init($('#slider'))
})


var xSlider = function(){
	return {
		ob: null,
		length:0,
		width:0,
		i:0,
		showItem:0,
		setParams: function(){
			var _self = this
			_self.showItem = 3
			if($(window).width() < 1060){
				_self.showItem = 2
			}
			if($(window).width() < 760){
				_self.showItem = 1
			}
			_self.i = 0;
			_self.ob.attr("show-item",_self.showItem)
			_self.ob.attr("i",_self.i)
			_self.ob.find('ul').css("left",0)
			_self.width = _self.ob.find(".cont").width();
		},
		init: function(_ob){
			var _self = this
			_self.ob = _ob;
			_self.setParams()
			_self.length = _self.ob.find("li").length
			_self.width = _self.ob.find(".cont").width();
			_self.i = 0;
			_self.showItem = _self.ob.attr("show-item") != undefined ? _self.ob.attr("show-item") : 1
			
			_self.ob.find(".right").click(function(){
				_self.i++;
				if(_self.i >= _self.length-_self.showItem)
					_self.i = _self.length-_self.showItem					
				_self.drive()
				return false
			})
			
			_self.ob.find(".left").click(function(){
				_self.i--;
				if(_self.i < 0)
					_self.i = 0					
				_self.drive()				
				return false
			})
				
			if(_self.length <= _self.showItem){
				_self.ob.find(".left,.right").hide()
			}
			$(window).resize(function(){
				_self.setParams()
			})
			//_self.initnav()
		},
		drive: function(){
			var _self = this
			_self.ob.find('ul').each(function(n){
				_self.showItem = _self.showItem*1 
				console.log(n, _self.i, _self.width, _self.showItem)
				$(this).stop().animate({
					left:((n-_self.i)*_self.width/_self.showItem)
				},300,function(){})
			})
			if(_self.i >= _self.length-_self.showItem){
				_self.ob.find(".right").addClass("hide")
			}else{
				_self.ob.find(".right").removeClass("hide")
			}
	
			if(_self.i <= 0){
				_self.ob.find(".left").addClass("hide")
			}else{
				_self.ob.find(".left").removeClass("hide")
			}
			_self.ob.attr("i",_self.i);
			_self.ob.find(".nav a").removeClass("act")
			_self.ob.find(".nav a").eq(_self.i).addClass("act")
		},
		initnav: function(){
			var _self = this
			_self.ob.append('<div class="nav"></div>');
			for(var i=1; i <=_self.length; i++){
				_self.ob.find(".nav").append('<a href="#">'+i+'</a>');
			}
			_self.ob.find(".nav a").eq(0).addClass("act")
			_self.ob.find(".nav a").click(function(){
				_self.ob.find(".nav a").removeClass("act")
				$(this).addClass("act")
				var i = _self.ob.find(".nav a").index($(this))
				_self.i = i					
				_self.drive()
				return false;
			})
		},
	}
}


function formRequest(o){
	ob = $(o)
	var send = true
	var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	
	ob.find(".err").removeClass("err")
	ob.find(".message").html("")

	var arFile = ["NAME","EMAIL"];
	for(var x in arFile){
		file = ob.find("[name='"+arFile[x]+"']")
		if(file.val() == ""){
			send = false;
			file.parents(".it").eq(0).addClass("err")
		}
	}
	/*
	file = ob.find("[name='EMAIL']")
	if(file.val() == "" || filter.test(file.val()) == false){
		send = false;
		file.parents(".it").eq(0).addClass("err")
	}*/
	
	
	if(send){
		var params = {
			"NAME":ob.find("[name='NAME']").val(),
			"EMAIL":ob.find("[name='EMAIL']").val(),
		}
		
		ob.find(".fields").css("opacity",0)
		ob.addClass("submited")
		
		$.ajax({
			  url: "/send.php",
			  data: params,
			  type: 'post',
			  success: function(data){
				openPopup('thank')
				ob.removeClass("submited")
				ob.find(".text").val("")
			  }
			});
	}

	return false;
}


function openPopup(id){
	$(".popup-message-fon").show()
	$(".popup-message").hide();
	$("#"+id).show()
	return false;
}

function initPopupMess(){
	$(".popup-message").find(".close").click(function(){
		$(".popup-message").hide()
		$(".popup-message-fon").hide()
		return false;
	});
	$(".popup-message-fon").click(function(){
		$(".popup-message").find(".close").trigger("click")
	})
}








function initScrollTo() {
	$(".up .scroll").click(function(){
		var h = $(window).height() > 700 ? 700 : $(window).height()
		$('html, body').animate({
			scrollTop: h
		}, 1000);
		return false;
	})
	$(".link-to-up a").click(function(){
		$('html, body').animate({
	        scrollTop: 0
	    }, 1000);
		return false;
	})
	$(window).scroll(function(){
		if($(window).scrollTop() < $(window).height()){
			$(".link-to-up a").hide()
		}else{
			$(".link-to-up a").show()
		}
	})
	$(".link-to-up a").hide()
}
